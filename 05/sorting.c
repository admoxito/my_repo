#include <stdio.h>
#include <stdint.h>



int main() {
  int massive[5] = {1, 3, 2, 10, 5};
  
  int i;
  int change;

  for (i = 0; i < 4; i++) {
    if (i + 1 < 5 && massive[i] > massive[i+1]) {
      change = massive[i];
      massive[i] = massive[i+1];
      massive[i+1] = change;       
    }
  }
  for (i = 0; i < 5; i++) {
    printf("%d ", massive[i]);
  }

  return 0;
}
