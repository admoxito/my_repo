#!/bin/bash

start=$(date +%S%N)

let "k=2**17"
a=0
while [ $a -lt $k ]; do
  let "t=2**$a"
  printf "$t \n" >> 1.txt
  a=$(($a+1))
done

end=$(date +%S%N)
func=$((($end-$start)/1000000000))
echo "$func"
