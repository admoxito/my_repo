class sort{
	public static void main(String[] args){
		int[] massive = new int[] {1, 3, 2, 5, 4};
		int change;

		for (int i = 0; i < massive.length-1; ++i){
			if (i + 1 < massive.length && massive[i] > massive[i+1]){
				change = massive[i];
				massive[i] = massive[i+1];
				massive[i+1] = change;
			}
		}
		for (int i = 0; i < massive.length; ++i){
			System.out.print(massive[i]);
		}
	}
}